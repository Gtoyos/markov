
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAXSIZE 100 // Max size of an individual word
#define MAXLEN 200 // Max length of a sentence
#define MAXFILEINPUT 100 //Maxnumber of files for input

#define HELPMSG "---Markov chain sentence generator.---\nDescription: Generate a new sentence based on a provided text.\nUse: markov (parameters) input...\n\
Parameters:\n\t-s n: generate n sentences\n\t-p n: generate n paragraphs\n\t-w n: max length of a sentence\n\t-f filename: save output in filename\nNote: Standard I/O redirection supported.\n"

typedef struct{
	int sentences;
	int words;
    int paragraphs;
    char * savefile;
} Vars;

typedef struct z{
	char * word;
	int count;
	struct z * nxt;
} Probnode;

typedef struct x{
	char * word;
	Probnode * nextwords;
} Wordnode;

typedef struct y{
	Wordnode * wordnode;
	struct y * nxt;
} Wordnodelist;

Vars params = {-1,MAXLEN,-1,NULL};

int getword(FILE *, char *);
Wordnodelist * newnode(char *, char *, Wordnodelist *);
void markovfree(Wordnodelist *);
Wordnodelist * markovload(FILE *, Wordnodelist *);
void markovgen(FILE *, Wordnodelist *);
Wordnode * getnode(char *, Wordnodelist *);
Wordnode * addword(char *, Wordnode *);
int rand_lim(int);
int debug_showmarkovchain(FILE *, Wordnodelist *);
int parseparams(int, char **,int[]);
void displayhelp();
int printparagraph(FILE *, Wordnodelist *);

int main(int argc, char * argv[]){
    Wordnodelist * list = NULL;
	int inputs[MAXFILEINPUT];
	int sen,presen;
	FILE *output, *input, *seedsrc;

	/* Initialize pseudorandom generation */
	seedsrc = fopen("/dev/urandom","r");
	srand((unsigned int) fgetc(seedsrc));
	fclose(seedsrc);

	parseparams(argc,argv,inputs);

	for(int i=0; inputs[i] != -1; i++){
		input = fopen(argv[inputs[i]],"r");
		if (input == NULL){
			fprintf(stderr,"ERROR: Can't open file %s.\n",argv[inputs[i]]);
			exit(2);
		}
		list = markovload(input,list);
		fclose(input);	
	}
    if (inputs[0] == -1) list = markovload(stdin,list);
	if (list == NULL){
		fprintf(stderr,"ERROR: Can't generate Markov chain. Check input.\n");
		exit(3);
	}
	(params.savefile == NULL) ? (output = stdout) : (output = fopen(params.savefile, "w"));
	if (output == NULL){
		fprintf(stderr,"ERROR: Can't save text in %s.\n",params.savefile);
		exit(2);
	}

	if (params.paragraphs != -1)
		printparagraph(output,list);
	else if (params.paragraphs == -1 && params.sentences != -1)
		for(int k=0; k < params.sentences; k++)
			markovgen(output,list);
	else
		markovgen(output,list);
	
	markovfree(list);
	if (output != stdout) fclose(output);
	if (params.savefile != NULL) free((void*)params.savefile);
	exit(0);
}

Wordnodelist * markovload(FILE * stream, Wordnodelist * list){ 
	/* Feeds a markov chain with data. */
	char next[MAXSIZE], word[MAXSIZE];
	Wordnode *wordptr;

	getword(stream, word);
	while(getword(stream, next) != -1){
		wordptr = getnode(word,list);
		if (wordptr == NULL)
			list = newnode(word,next,list);
		else
			addword(next,wordptr);
		strcpy(word,next);
	}
	return list;
}

Wordnode * getnode(char * word, Wordnodelist * list){
	/* Returns pointer to word in the list. Returns NULL if there is no match. */
	while (list != NULL){
		if (!strcmp(word,list->wordnode->word))
			break;
		else
			list = list->nxt;
	}
	if (list == NULL)
		return NULL;
	else
		return list->wordnode;
}

Wordnodelist * newnode(char * word, char * next, Wordnodelist * list){
	/* Creates a new node for a word in the node list. Returns pointer to the new node. */
	Wordnode * new = (Wordnode *)malloc(sizeof(Wordnode));
	Wordnodelist * base = list;
	
	new->nextwords = (Probnode *)malloc(sizeof(Probnode));
	new->nextwords->count = 1;
	new->nextwords->nxt = NULL;
	new->nextwords->word = (char *)malloc(strlen(next)+1);
	new->word = (char *)malloc(strlen(word)+1);
	strcpy(new->word,word);
	strcpy(new->nextwords->word,next);

	if (list == NULL){
		list = (Wordnodelist *) malloc(sizeof(Wordnodelist));
		list->nxt = NULL;
		list->wordnode = new;
		return list;
	}
	else{
		while(list->nxt != NULL)
			list = list->nxt;
		list->nxt = (Wordnodelist *)malloc(sizeof(Wordnodelist));
		list->nxt->wordnode = new;
		list->nxt->nxt = NULL;
		return base;
	}
}

Wordnode * addword(char * next, Wordnode * node){
	/* Adds a new word to the wordnode */
	Probnode *tmp1 = node->nextwords;
	while(tmp1->nxt != NULL)
		if (strcmp(tmp1->word,next))
			tmp1 = tmp1->nxt;
		else break;
	if (!strcmp(tmp1->word,next))
		(tmp1->count)++;
	else{
		tmp1->nxt = (Probnode *) malloc(sizeof(Probnode));
		tmp1->nxt->word = (char *) malloc(strlen(next)+1);
		strcpy(tmp1->nxt->word,next);
		tmp1->nxt->count = 1;
		tmp1->nxt->nxt = NULL;
	}
}

void markovfree(Wordnodelist * base){	
    /* Fully frees a markov list*/
    Wordnodelist *p,*t = base;
	Probnode *tmp1,*tmp2;
    int i;

    while(t != NULL){
        free((void *)t->wordnode->word);
        tmp1 = t->wordnode->nextwords;
		while(tmp1 != NULL){
			tmp2 = tmp1->nxt;
			free((void *)tmp1->word);
			free((void *)tmp1);
			tmp1 = tmp2;
		}
        free((void *)t->wordnode);
        p = t;
        t = t->nxt;
        free((void *)p);
    }
}

void markovgen(FILE * stream, Wordnodelist * list){
	/* Generates a markov chain sentence */
	int n,nodecount=0,nextcount,senlen=0;
	register int firstnode=1;
	char *nextw, *str;
	Wordnodelist *seed;
	Wordnode *w;
	Probnode *p;
	FILE *seedsrc;

	seed = list;
	
	while(seed->nxt != NULL){
		seed = seed->nxt;
		nodecount++;
	}

	seed = list;
	for(int i = rand_lim(nodecount); i > 0; i--)
		seed = seed->nxt;

	w = seed->wordnode;
	while(strcmp(w->word,".") && strcmp(w->word,"!") && strcmp(w->word,"?") && senlen < params.words-1){
		if (firstnode){
			str = malloc(strlen(w->word)+1);
			strcpy(str,w->word);
			str[0] = toupper(str[0]);
			fprintf(stream,"%s",str);
			free((void *)str);
			firstnode = 0;
		}
		else
			fprintf(stream,"%s",w->word);

		nextcount = 0;
		p = w->nextwords;
		while(p != NULL){
			nextcount += p->count;
			p = p->nxt;
		}

		n = 0;
		p = w->nextwords;
		nextw = p->word;
		for(int i = rand_lim(nextcount); i > 0; i--){
			n++;
			if (n > p->count){
				n = 0;
				p = p->nxt;
			}
			nextw = p->word;
		}

		w = getnode(nextw,list);
		if (strcmp(w->word,".") && strcmp(w->word,"!") && strcmp(w->word,"?"))
			fputc(' ',stream);
		senlen++;
	}
	fprintf(stream,"%s",w->word);
	if (senlen == params.words-1 && (strcmp(w->word,".") && strcmp(w->word,"!") && strcmp(w->word,"?") )) fputc('.',stream);
	if (params.paragraphs == -1)
		fputc('\n',stream);
}

int getword(FILE * stream, char * word){
	/* Gets next word from stream. Returns length of word including '\0'. -1 at EOF */
	int c,i=0;

	getnextchar:
	while ( i < MAXSIZE-1 && ((c = fgetc(stream)) != EOF) && c != ' ' && c != '.' && c != '!' && c!= '?' && c != '\n' && c != '\t')
		word[i++] = c;
	if (c == '.' || c == '?' || c == '!')
		if (i > 0)
			ungetc(c,stream);
		else
			word[i++] = c;
	else if (c == EOF)
		if (i > 0)
			ungetc(c,stream);
		else
			return -1;
	else if (i == 0)
		goto getnextchar;
	word[i] = '\0';
	return i;
}

int rand_lim(int limit){
	/* return a linearly distributed random number between 0 and limit inclusive. */
	/* https://stackoverflow.com/questions/2999075/generate-a-random-number-within-range/ */

    int divisor = RAND_MAX/(limit+1);
    int retval;

    do { 
        retval = rand() / divisor;
    } while (retval > limit);
    return retval;
}

int debug_showmarkovchain(FILE * stream, Wordnodelist * base){
	/*DEBUG: Shows the markov data structure tree */
	Wordnodelist *list= base;
	Probnode *t;

	while(list != NULL){
		fprintf(stream,"%s:\n",list->wordnode->word);
		t = list->wordnode->nextwords;
		while(t != NULL){
			putc('\t',stream);
			fprintf(stream,"%s (%d)\n",t->word,t->count);
			t =  t->nxt;
		}
		list = list->nxt;
	}
	return 0;
}

int parseparams(int argc, char **argv,int inputs[]){
    int j,i=1,k=0;
    char *str = NULL;

    inputs[k] = -1;

    while (argc-- > 1){
        if (argv[i][0] == '-'){
            for (j=1; argv[i][j]; j++){
                switch (argv[i][j]){
                    case 'h':
                        displayhelp();
                        exit(0);
                        break;
                    case 'w':
                        i++;
						argc--;
						params.words = atoi(argv[i]);
						argv[i][j--] = '\0';
                        break;
                    case 'p':
                        i++;
						argc--;
						params.paragraphs = atoi(argv[i]);
						argv[i][j--] = '\0';
                        break;
                    case 'f':
                        i++;
						argc--;
						str = (char *)malloc(strlen(argv[i])+1);
						strcpy(str,argv[i]);
						params.savefile = str;
						argv[i][j--] = '\0';
                        break;
                    case 's':
                        i++;
						argc--;
						params.sentences = atoi(argv[i]);
						argv[i][j--] = '\0';
                        break;
                    default:
                        return 1;
                        break;
                }
            }
        }
        else
            inputs[k++] = i;
        i++;
    }
    inputs[k] = -1;
    return 0;
}

void displayhelp(){
	fprintf(stderr,HELPMSG);
}

int printparagraph(FILE * stream, Wordnodelist * list){
	/* Prints a paragraph in stream */
	const int paraprob[] = {5,5,5,20,30,20,5,5,5}; // Distribution of paragraph size.
	int k,presen;

	for (int i=0; i < params.paragraphs; i++){
		if (params.sentences == -1){
			presen = rand_lim(100);
			for (k=0; presen > 0; k++)
				presen -= paraprob[k];
			presen = k+3;
		}
		else
			presen = params.sentences;

		for (int j=0; j < presen; j++){
			markovgen(stream,list);
			fputc(' ',stream);
		}
		fprintf(stream,"\n\n");
	}
}
