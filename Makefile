CC = gcc
TARGET = aimg

markov: main.c
	$(CC) -o $@ $<

.PHONY: install
install:
	mv markov /usr/bin/
