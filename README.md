# Markov

Markov chain sentence generator. Generates a new sentence based on a provided text

## Installation
Use:
``make``
and
``make install``
to compile & install markov in /usr/bin

## Usage

Syntax: ``markov (parameters) inputfiles...``

Parameters:

* ``-w n`` Maximum length allowed for a sentence.
* ``-s n`` Generate n sentences.
* ``-p n`` Generate n paragraphs. 
* ``-f filename`` Save output in filename.

Use ``markov -h`` for more info.

### Compatibility

Markov should run on any UNIX-like environment.
The software has been developed on Debian GNU/Linux

### Contributing

Feel free to make pull requests. All ideas will be well recieved.

### License

This project is licensed under the GPLv3.0 License - see the [LICENSE.md](LICENSE.md) file for details.
